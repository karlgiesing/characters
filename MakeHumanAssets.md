# MakeHuman Assets
Notes about the community assets I use in MakeHuman, including license and author information.

## By Character

### FemaleAsian1

**Geometries**

* Deathnote
    ```
    deathnote t-shirt
    author:  o4saken/punkduck
    license: CC-BY
    ```
    (Note: Probably can't use due to trademark issues)
* Halfboots
    ```
    female half-boots
    author:  punkduck
    license: CC-BY
    ```
* Lacechoker
    ```
    lace-choker
    author:  punkduck
    license: CC0
    ```
* Tightjeans
    ```
    Tight Jeans (female)
    author:  punkduck
    license: CC-BY
    ```
* Frenchbraid
    ```
    French Braid 01 Variation
    author:  Elvaerwyn
    license: CC0
    ```

**Materials**

* skin: Young Asian Female - MakeHuman
* hair: Frenchbraid
    ```
    French Braid 01 Variation
    author:  Elvaerwyn
    license: CC0
    ```
* eyes: Brownlight - MakeHuman

### FemaleNativeAmerican1

**Geometries**

* string4
    ```
    String 4
    author:  punkduck
    license: CC-BY
    ```
* Vintagetop
    ```
    Retro Top
    author:  punkduck
    license: CC-BY
    ```
* Shoes01 - MakeHuman
* Namfashion...?
    ```
    Necklace (Native American Fashion)
    author:  punkduck
    license: CC-BY
    ```
* Namfashion...?
    ```
    Skirt (Native American Fashion)
    author:  punkduck
    license: CC-BY
    ```
* Double mhbraid
    ```
    Double MH Braid 01
    author:  Elvaerwyn
    license: CC0
    ```

**Materials**

* skin: Young Hispanic Female EHSP 001
    ```
    Eve Hill's Young_Hispanic_Female
    author:  Eve Hill Sisters Production Company/3d.sk/jimlsmith9999
    license: CC-BY
    ```
    Note: this might not actually be allowed for reuse under CC-BY - use with caution
* hair: Double mhbraid
    ```
    French Braid 01 Variation
    author:  Elvaerwyn
    license: CC0
    ```
* eyes: Brown - MakeHuman

### FemaleSexy
_(Note: probably don't need any of these, won't be adding her to the game)_

**Geometries**

* Armsleeve fishnet medium
    ```
    [LINGERIE] Armsleeves Black Fishnet Medium
    author:  V0rT3x
    license: CC0
    ```
* Bandeau bra
    ```
    Bandeau Bra
    author:  punkduck
    license: CC-BY
    ```
* Belt4-male-cjeans
    ```
    male classic jeans (belt)
    author:  punkduck
    license: CC-BY
    ```
* Blackminiskirt
    ```
    Black Mini Skirt
    author:  punkduck
    license: CC-BY
    ```
* Kneeboots
    ```
    female knee boots
    author:  punkduck
    license: CC-BY
    ```
* Lacechoker
    ```
    lace-choker
    author:  punkduck
    license: CC0
    ```
* Stockings fishnet medium
    ```
    [LINGERIE] Stockings Black Fishnet Medium
    author:  V0rT3x
    license: CC0
    ```
* Transparent bra
    ```
    Transparent Bra
    author:  punkduck
    license: CC-BY
    ```
* long2 alpha7 (hair)
    ```
    Hairstyle Long2 (alpha 7 adaption)
    author:  punkduck/MakeHuman
    license: CC-BY
    ```

**Materials**

* skin: Creamy female
    ```
    creamy_female
    author:  skaaldyrssuppe
    license: CC0
    ```
* hair: long2 alpha7
    ```
    Hairstyle Long2 (alpha 7 adaption)
    author:  punkduck/MakeHuman
    license: CC-BY
    ```
* eyes: Brown - MakeHuman

### FemaleZombieOffice

**Geometries**

* Female elegantsuit - MakeHuman
* Shoes01 - MakeHuman
* string5
    ```
    string 5
    author:  punkduck
    license: CC-BY
    ```
* ponytail01 - MakeHuman
* Teeth base - MakeHuman
* 

**Materials**

* skin: Sohh Zombie Female
    ```
    sohh Female zombie skin
    author:  sohh
    license: CC0
    ```
* hair: ponytail01 - MakeHuman
* eyes: Zombie eyes
    ```
    Zombie eyes
    author:  culturalibre
    license: CC0
    ```

### Harlot
_(Note: probably don't need any of these, won't be adding her to the game)_

**Geometries**

* Dagger
    ```
    Dagger
    author:  o4saken
    license: CC0
    ```
* Elv maidbonnet1
    ```
    Elvs Dirty Lil Maid Bonnet
    author:  Elvaerwyn
    license: CC-BY
    ```
* Fr stockings
    ```
    french lingerie (stockings)
    author:  punkduck
    license: CC-BY
    ```
* Frenchbra
    ```
    french lingerie (bra)
    author:  punkduck
    license: CC-BY
    ```
* Heelsandal
    ```
    heel sandals
    author:  punkduck
    license: CC-BY
    ```
* Lacechoker
    ```
    lace-choker
    author:  punkduck
    license: CC0
    ```
* Leather corset
    ```
    leather corset
    author:  punkduck
    license: CC-BY
    ```
* Longgloves
    ```
    evening gloves
    author:  punkduck
    license: CC-BY
    ```
* Nosepierce
    ```
    Nose piercing
    author:  Joel Palmius
    license: CC0
    ```
* String3
    ```
    french lingerie (mini briefs)
    author:  punkduck
    license: CC-BY
    ```
* Suspender belt
    ```
    french lingerie (suspender-belt)
    author:  punkduck
    license: CC-BY
    ```

**Materials**

* skin: Eves Young Light Skin Caucasian Female
    ```
    Eves Young Light Skin Caucasian Female
    author:  Eve Hill Sisters Production Company/3d.sk/jimlsmith9999
    license: CC-BY
    ```
    Note: this might not actually be allowed for reuse under CC-BY - use with caution
* eyes: Makehuman eye diffuse grey
    ```
    Diffuse Grey Eyes
    author:  bobby_03
    license: CC0
    ```

### MaleBlackOld

**Geometries**

* M trilby hat
    ```
    Elvs Male Trilby Hat
    author:  Elvaerwyn
    license: CC-BY
    ```
* Male casualsuit01 - MakeHuman
* Shoes03 - MakeHuman

**Materials**

* skin: Middleage african male
    ```
    skin male african middleage
    author:  Mindfront
    license: CC0
    ```
* eyes: Brown - MakeHuman
* eyebrows: Eyebrow0o3 - MakeHuman

### MaleBlackOldMilitary

**Geometries**

* Harvey pantsboots
    ```
    Harvey_PantsBootsV1
    author:  callharvey3d
    license: CC-BY
    ```
* M wifebeater elv1
    ```
    Elvs Male Athletic Tank1
    author:  Elvaerwyn
    license: CC-BY
    ```
* Patrol cap
    ```
    Patrol Cap
    author:  Mindfront
    license: CC-BY
    ```

**Materials**

* skin: Middleage african male
    ```
    skin male african middleage
    author:  Mindfront
    license: CC0
    ```
* eyes: Brown - MakeHuman
* eyebrows: Eyebrow003 - MakeHuman

### MaleLeatherMohawk

**Geometries**

* Belt4-male-cjeans
    ```
    male classic jeans (belt)
    author:  punkduck
    license: CC-BY
    ```
* Elvs leatherjacket
    ```
    Elvs Ladies Leather Jacket
    author:  Elvaerwyn
    license: CC-BY
    ```
* Eyepatch
    ```
    Eyepatch
    author:  culturalibre
    license: CC0
    ```
* Harvey hawk1
    ```
    Harvey_Mohawk1
    author:  callharvey3d
    license: CC-BY
    ```
* motorcyclepants
    ```
    Motorcycle Pants
    author:  punkduck
    license: CC-BY
    ```
* Nosepierce
    ```
    Nose piercing
    author:  Joel Palmius
    license: CC0
    ```
* Shoes biker boots male
    ```
    Shoes Biker Boots Male
    author:  Mindfront
    license: CC-BY
    ```

**Materials**

* skin: Character Aksel Skin
    ```
    Aksel Skin
    author:  Mindfront
    license: CC0
    ```
* eyes: Brown - MakeHuman
* eyebrows: Eyebrow003 - MakeHuman

### MaleNativeAmericanOld

**Geometries**

* Male casualsuit05 - MakeHuman
* Shoes03 - MakeHuman
* Long alpha7 (hair)
    ```
    Hairstyle Long (alpha 7 adaption)
    author:  punkduck/MakeHuman
    license: CC-BY
    ```

**Materials**

* skin: Mohawk
    ```
    Mohawk
    author:  sssnake
    license: CC0
    ```
* hair: Long alpha7
    ```
    Hairstyle Long (alpha 7 adaption)
    author:  punkduck/MakeHuman
    license: CC-BY
    ```
* eyes: Brown - MakeHuman
* eyebrows: Eyebrow003 - MakeHuman

### ZombieFemaleStripper

**Geometries**

* Armsleeve fishnet small
    ```
    [LINGERIE] Armsleeves Black Fishnet Small
    author:  V0rT3x
    license: CC0
    ```
* Belt4-male-cjeans
    ```
    male classic jeans (belt)
    author:  punkduck
    license: CC-BY
    ```
* Lacechoker
    ```
    lace-choker
    author:  punkduck
    license: CC0
    ```
* Nosepierce
    ```
    Nose piercing
    author:  Joel Palmius
    license: CC0
    ```
* Shoes biker boots female
    ```
    Shoes Biker Boots Female
    author:  Mindfront
    license: CC-BY
    ```
* string4
    ```
    String 4
    author:  punkduck
    license: CC-BY
    ```

**Materials**

* skin: Sohh Zombie Female
    ```
    sohh Female zombie skin
    author:  sohh
    license: CC0
    ```
* hair: Straight bangs
    ```
    Straight Bangs
    author:  cortu
    license: CC0
    ```
* eyes: Zombie eyes
    ```
    Zombie eyes
    author:  culturalibre
    license: CC0
    ```
