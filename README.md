# Characters

Non-player characters created by me, mainly designed for use in modifications to
[7 Days to Die](https://7daystodie.com).

## Tools Used
This is a list of the tools used to create these characters.

This repository does _not_ contain either the tools themselves,
nor the assets that are packaged with them.

### Modeling and Texturing

* [Mixamo Fuse 1.3](https://store.steampowered.com/app/257400/Fuse) - 
    a free (as in beer) program for making human-like characters, including zombies.
    Unfortunately, after Adobe bought it, it was discontinued and is no longer supported.
    But it can still be used, and is still very good.
* [MakeHuman](http://www.makehumancommunity.org) -
    an open source program for making human characters.
    It includes access to many community assets.

### Rigging and Animation

* [Mixamo](https://www.mixamo.com) - a website that will rig your characters,
    and optionally add animations.
    Includes many free animations (and many that are paid).

### Game Engine

* [Unity](https://unity.com)

## Directory structure

* `Models` - models in the program's native format, plus exported `.obj` and texture files
    * `FromMixamo` - `.fbx` files after rigging/animating in Mixamo; no textures included
    * `Fuse` - characters created in the Fuse program
    * `MakeHuman` - characters created in the MakeHuman program
* `Unity` - Cutom Unity files.
    It should contain only files that I am legally allowed to distribute;
    if not please let me know and I will remove them.
  * `Assets` - custom Unity assets for use in character creation.
    * `Scripts` - C# scripts that I have created or modified.
    * `Shaders` - Unity shaders that I have created or modified.

## License

Any rights that I hold in these characters, I hereby place into the public domain, under a
[CC0](https://creativecommons.org/share-your-work/public-domain/cc0) license.

However, I don't hold the rights to all the assets.
I have used assets from Mixamo Fuse and MakeHuman, and do not hold any rights in those assets.
Those rights are retained by Fuse, and/or the members of the MakeHuman community.

This sounds worse than it is.
Both the Fuse and MakeHuman assets are released under very lenient licenses:

* Characters created in Mixamo Fuse may be used in any game (commerical or not) for free,
    though you cannot repackage and sell the assets.
* MakeHuman characters can include assets from the MakeHuman team, or community assets.
    The MakeHuman assets are CC0, and the community assets are usually CC0 or CC-BY.
    I believe I used CC0 assets in these models, but if not,
    contact me and I will either give credit or remove the character, as appropriate.

The `Unity` folder also contains custom assets, and most of these were not created from scratch.
They are mostly code, and the credits/usage rights should be in the file comments.
