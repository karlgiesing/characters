using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;

/// <summary>
/// From: https://gist.github.com/radiatoryang/3707b42341f6f7b3aa67b8387e1f8e68
/// </summary>
[ExecuteInEditMode]
public class MixamoMeshCombiner : MonoBehaviour
{
	/// <summary>
	/// You can leave this empty if you want it autodetect SMRs in children.
	/// </summary>
	public SkinnedMeshRenderer[] smRenderers;

	/// <summary>
	/// this is for stuff like Mixamo hair, where I find I often have to type in my own Bounds or
	/// else the hair disappears randomly because of weird bounds that don't calculate well.
	/// </summary>
	public Bounds boundsOverride;

    public bool ignoreBlendshapes = false;

	[SerializeField] bool doRestoreBindPose = true;

	[ContextMenu("Combine!")]
	public void StartCombining()
	{
		Combine();
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
    }

	// based on some code by JoeStrout https://forum.unity3d.com/threads/mesh-bindposes.383752/
	public void RestoreBindPose()
	{
		// if you're using Mixamo models, every SkinnedMeshRenderer has a fraction of the full bind pose data...
		// so first we have to search through all of the SMRs and combine all the bindpose data into a dictionary
		var smRenderers2 = transform.parent.GetComponentsInChildren<SkinnedMeshRenderer>().OrderBy(smr => smr.bones.Length).ToArray();

		Dictionary<Transform, Matrix4x4> bindPoseMap = new Dictionary<Transform, Matrix4x4>();

		foreach (var smr in smRenderers2)
		{
			for (int i = 0; i < smr.bones.Length; i++)
			{
				if (!bindPoseMap.ContainsKey(smr.bones[i]))
				{
					bindPoseMap.Add(smr.bones[i], smr.sharedMesh.bindposes[i]);
				}
			}
		}

		// based on data, now move the bones based on the bindPoseMap
		foreach (var kvp in bindPoseMap)
		{
			Transform boneTrans = kvp.Key;
			Matrix4x4 bindPose = kvp.Value;

			// Recreate the local transform matrix of the bone
			Matrix4x4 localMatrix = bindPoseMap.ContainsKey(boneTrans.parent) ? (bindPose * bindPoseMap[boneTrans.parent].inverse).inverse : bindPose.inverse;
			// Recreate local transform from that matrix
			boneTrans.localPosition = localMatrix.MultiplyPoint(Vector3.zero);
			boneTrans.localRotation = Quaternion.LookRotation(localMatrix.GetColumn(2), localMatrix.GetColumn(1));
			boneTrans.localScale = new Vector3(localMatrix.GetColumn(0).magnitude, localMatrix.GetColumn(1).magnitude, localMatrix.GetColumn(2).magnitude);
		}
		Debug.Log("Reset " + bindPoseMap.Count + " bones to bind pose");
	}

	// internal vars for blendshapes and boneweights etc
	class BlendShapeFrame
	{
		public string name;
		public float weight;
		public List<Vector3> deltaVertices = new List<Vector3>();
		public List<Vector3> deltaNormals = new List<Vector3>();
		public List<Vector3> deltaTangents = new List<Vector3>();
	}

	static List<List<BlendShapeFrame>> blendFrames = new List<List<BlendShapeFrame>>();
	static string[] blendshapeNames = new string[] { "Blink_Left", "Blink_Right", "BrowsDown_Left",
		"BrowsDown_Right", "BrowsIn_Left", "BrowsIn_Right", "BrowsOuterLower_Left", "BrowsOuterLower_Right",
		"BrowsUp_Left", "BrowsUp_Right", "CheekPuff_Left", "CheekPuff_Right", "EyesWide_Left",
		"EyesWide_Right", "Frown_Left", "Frown_Right", "JawBackward", "JawForward",
		"JawRotateY_Left", "JawRotateY_Right", "JawRotateZ_Left", "JawRotateZ_Right", "Jaw_Down",
		"Jaw_Left", "Jaw_Right", "Jaw_Up", "LowerLipDown_Left", "LowerLipDown_Right",
		"LowerLipIn", "LowerLipOut", "Midmouth_Left", "Midmouth_Right", "MouthDown",
		"MouthNarrow_Left", "MouthNarrow_Right", "MouthOpen", "MouthUp", "MouthWhistle_NarrowAdjust_Left",
		"MouthWhistle_NarrowAdjust_Right", "NoseScrunch_Left", "NoseScrunch_Right", "Smile_Left", "Smile_Right",
		"Squint_Left", "Squint_Right", "TongueUp", "UpperLipIn", "UpperLipOut",
		"UpperLipUp_Left", "UpperLipUp_Right" };
	static Vector3[] deltaVertices, deltaNormals, deltaTangents;
	static List<BoneWeight> boneWeights = new List<BoneWeight>();
	static List<CombineInstance> combineInstances = new List<CombineInstance>();
	static BoneWeight[] meshBoneweight;

	void Combine()
	{
        // the initial state of the model matters a lot
        // so let's turn off the animator (just in case) and ideally try to reset the model back to bindpose
        var anim = GetComponentInParent<Animator>();
		Debug.Log("anim: " + anim); 
		anim.enabled = false;

		if (smRenderers == null || smRenderers.Length == 0)
			smRenderers = GetComponentsInChildren<SkinnedMeshRenderer>();

		if (smRenderers == null || smRenderers.Length == 0)
        {
			Debug.Log("No skinned mesh renderers found!");
			return;
		}

		if (doRestoreBindPose)
		{ 
			RestoreBindPose();
		}

		bool doBlendshapes = !ignoreBlendshapes && smRenderers[0].sharedMesh.blendShapeCount > 0;

		Vector3 oldPos = transform.position;
		Quaternion oldRot = transform.rotation;

		// reset the model to 0,0,0 (temporarily) so we can work with it easier
		transform.position = Vector3.zero;
		transform.rotation = Quaternion.identity;

		List<Transform> bones = new List<Transform>();
		boneWeights.Clear();
		combineInstances.Clear();

        smRenderers = smRenderers.Where(x => x.gameObject.activeSelf).ToArray();
		if (smRenderers.Length == 0)
		{
			Debug.Log("No skinned mesh renderers with active objects");
			return;
		}

		var mat = smRenderers[0].sharedMaterial;
		int numSubs = 0;
		foreach (SkinnedMeshRenderer smr in smRenderers)
		{
			numSubs += smr.sharedMesh.subMeshCount;
		}

		int[] meshIndex = new int[numSubs];
		int boneOffset = 0;

		// bone map stuff
		var boneMap = new Dictionary<string, int>();
		var allBones = new List<Transform>();
        //if (rootBoneCombine != null)
        //{
        //    allBones = rootBoneCombine.GetComponentsInChildren<Transform>().ToList();
        //    for (int i = 0; i < allBones.Count; i++)
        //    {
        //        boneMap.Add(allBones[i].name, i);
        //    }
        //}

        blendFrames.Clear();

		var castShadows = smRenderers[0].shadowCastingMode;
		var receiveShadows = smRenderers[0].receiveShadows;

		// "BODY" MUST BE THE FIRST SMRENDERER in your smRenderer array, BECAUSE IT HAS *EVERY* BLENDSHAPE AND BLEND FRAME
		for (int s = 0; s < smRenderers.Length; s++)
		{
			SkinnedMeshRenderer smr = smRenderers[s];

			var sMesh = smr.sharedMesh;

			// blend shape stuff
			// mixamo meshes sometimes have different blendshape counts and indices (WTF???)
			bool doNameSearch = s >= 1 && sMesh.blendShapeCount > 0 && sMesh.blendShapeCount < 50;

			// hardcoded to Mixamo's 50 because even meshes without blendshapes will need zeroed deltas when they get merged
			for (int shapeIndex = 0; shapeIndex < 50 && doBlendshapes; shapeIndex++)
			{
				// runs only on first mesh, allocates space and sets up structure
				if (blendFrames.Count == shapeIndex)
				{
					var shapeName = "Facial_Blends." + blendshapeNames[shapeIndex];
					var frameCount = sMesh.GetBlendShapeFrameCount(shapeIndex);
					blendFrames.Add(new List<BlendShapeFrame>());
					for (int frameIndex = 0; frameIndex < frameCount; frameIndex++)
					{
						var newFrame = new BlendShapeFrame();
						newFrame.name = shapeName;
						newFrame.weight = sMesh.GetBlendShapeFrameWeight(shapeIndex, frameIndex);
						blendFrames[shapeIndex].Add(newFrame);
					}

				}

				// runs on every mesh, adds the deltas for each frame... if there are no deltas, then it adds empty deltas
				for (int frameIndex = 0; frameIndex < blendFrames[shapeIndex].Count; frameIndex++)
				{
					deltaVertices = new Vector3[sMesh.vertexCount];
					deltaNormals = new Vector3[sMesh.vertexCount];
					deltaTangents = new Vector3[sMesh.vertexCount];

					// search by blendshape name sometimes, because sometimes some meshes don't have all the blendshapes
					int actualShapeIndex = shapeIndex;
					if (doNameSearch)
					{
						actualShapeIndex = -1;
						if (sMesh.blendShapeCount > shapeIndex)
						{
							string groupName = sMesh.GetBlendShapeName(shapeIndex);
							for (int i = 0; i < blendFrames.Count; i++)
							{
								if (blendFrames[i][0].name == groupName)
								{ // does this iterated frameGroup's shapeName equal this current frameGroup's shapeName?
									actualShapeIndex = i;
									break;
								}
							}
						}
					}
					// only fetch deltas if there's a blendShapeFrame there
					int actualFrameCount = actualShapeIndex > -1 && sMesh.blendShapeCount > actualShapeIndex ? sMesh.GetBlendShapeFrameCount(actualShapeIndex) : 0;

					if (actualShapeIndex >= 0 && frameIndex < actualFrameCount)
					{
						sMesh.GetBlendShapeFrameVertices(shapeIndex, frameIndex, deltaVertices, deltaNormals, deltaTangents);
					}

					var frame = blendFrames[shapeIndex][frameIndex];
					frame.deltaVertices.AddRange(deltaVertices);
					frame.deltaNormals.AddRange(deltaNormals);
					frame.deltaTangents.AddRange(deltaTangents);
				}
			} // end blendshape merging

			// bone weight stuff
			meshBoneweight = smr.sharedMesh.boneWeights;
			var smrBones = smr.bones;

			// May want to modify this if the renderer shares bones as unnecessary bones will get added.
			foreach (BoneWeight bw in meshBoneweight)
			{
				BoneWeight bWeight = bw;

				if (boneMap.Count == 0)
				{
					bWeight.boneIndex0 += boneOffset;
					bWeight.boneIndex1 += boneOffset;
					bWeight.boneIndex2 += boneOffset;
					bWeight.boneIndex3 += boneOffset;
				}
				else
				{
					bWeight.boneIndex0 = boneMap[smrBones[bWeight.boneIndex0].name];
					bWeight.boneIndex1 = boneMap[smrBones[bWeight.boneIndex1].name];
					bWeight.boneIndex2 = boneMap[smrBones[bWeight.boneIndex2].name];
					bWeight.boneIndex3 = boneMap[smrBones[bWeight.boneIndex3].name];
				}

				boneWeights.Add(bWeight);
			}
			boneOffset += smr.bones.Length;

			if (boneMap.Count == 0)
			{
				Transform[] meshBones = smr.bones;
				foreach (Transform bone in meshBones)
					bones.Add(bone);
			}

            var ci = new CombineInstance
            {
                mesh = smr.sharedMesh,
				transform = smr.transform.localToWorldMatrix
			};
            meshIndex[s] = ci.mesh.vertexCount;
			combineInstances.Add(ci);

			// Set the game object to be inactive after the SMRs are combined
			smr.gameObject.SetActive(false);
		}

		List<Matrix4x4> bindposes = new List<Matrix4x4>();

		if (allBones.Count > 0)
			bones = allBones;

		for (int b = 0; b < bones.Count; b++)
		{
			bindposes.Add(bones[b].worldToLocalMatrix * transform.worldToLocalMatrix);
		}

		// begin constructing new mesh
		var newMesh = new Mesh();
		newMesh.CombineMeshes(combineInstances.ToArray(), true, true);

		// add blendshape frames
		for (int frameGroup = 0; frameGroup < blendFrames.Count; frameGroup++)
		{
			for (int frame = 0; frame < blendFrames[frameGroup].Count; frame++)
			{
				var thisFrame = blendFrames[frameGroup][frame];
				newMesh.AddBlendShapeFrame(
					thisFrame.name,
					thisFrame.weight,
					thisFrame.deltaVertices.ToArray(),
					thisFrame.deltaNormals.ToArray(),
					thisFrame.deltaTangents.ToArray());
			}
		}

		SkinnedMeshRenderer r = GetComponent<SkinnedMeshRenderer>();
		if (r == null) 
		{ 
			r = gameObject.AddComponent<SkinnedMeshRenderer>(); 
		}

		r.sharedMesh = newMesh;

		// TODO try saving the mesh now
		SaveMesh(r.sharedMesh, r.gameObject.name + "_mesh");

		// workaround for Unity bug 824384 where blendshapes would not update properly at runtime...
		// thanks Julius!
		SkinnedMeshRenderer smrr = r;
		Mesh m = smrr.sharedMesh;
		r.sharedMesh = m;

		r.shadowCastingMode = castShadows;
		r.receiveShadows = receiveShadows;

		r.sharedMaterial = mat;

		r.bones = bones.ToArray();
		r.sharedMesh.boneWeights = boneWeights.ToArray();
		r.sharedMesh.bindposes = bindposes.ToArray();

		r.sharedMesh.RecalculateBounds();

		// turn animator back on
		anim.enabled = true;

		// Reset the transform position/rotation
		transform.position = oldPos;
		transform.rotation = oldRot;
	}

	public static void SaveMesh(Mesh mesh, string name)
	{
		string path = EditorUtility.SaveFilePanel("Save Combined Mesh Asset", "Assets/", name, "asset");
		if (string.IsNullOrEmpty(path))
			return;

		path = FileUtil.GetProjectRelativePath(path);

		AssetDatabase.CreateAsset(mesh, path);
		AssetDatabase.SaveAssets();
	}
}